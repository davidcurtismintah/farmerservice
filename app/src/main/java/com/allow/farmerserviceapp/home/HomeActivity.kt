package com.allow.farmerserviceapp.home

import android.content.Context
import android.content.Intent
import android.location.Address
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.allow.farmerserviceapp.home.location.LocationFragment
import com.allow.farmerserviceapp.R
import com.allow.farmerserviceapp.home.statistics.StatisticsFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(),
    StatisticsFragment.OnFragmentInteractionListener,
    LocationFragment.OnFragmentInteractionListener {

    companion object {
        fun startIntent(ctx: Context) = Intent(ctx, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(toolbar)
        supportActionBar?.run {
            setDisplayShowHomeEnabled(false)
            setDisplayShowTitleEnabled(true)
        }

        container.setPagingEnabled(false)
        container.offscreenPageLimit = 1
        val sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        container.adapter = sectionsPagerAdapter
        showStatisticsView()
    }

    override fun onBackPressed() {
        if (container.currentItem == 1){
            showStatisticsView()
        } else {
            super.onBackPressed()
        }
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment = when(position){
            0 -> StatisticsFragment.newInstance()
            1 -> LocationFragment.newInstance()
            else -> StatisticsFragment.newInstance()
        }

        override fun getCount() = 2
    }

    fun showStatisticsView() {
        container.setCurrentItem(0, false)
        title = getString(R.string.statistic_fragment_title)
    }

    override fun showAddLocationView() {
        container.setCurrentItem(1, false)
        title = getString(R.string.location_fragment_title)
//        startActivity(LocationActivity.startIntent(this))
    }

    override fun showStatisticsView(address: Address) {
        val fragments = supportFragmentManager.fragments
        for (fragment in fragments){
            if (fragment is StatisticsFragment){
                fragment.showLocationOnMap(address)
                onBackPressed()
            }
        }
    }

}
