package com.allow.farmerserviceapp.home.statistics

import android.content.Context
import android.location.Address
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.allow.farmerserviceapp.R
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.enums.Anchor
import com.anychart.enums.HoverMode
import com.anychart.enums.Position
import com.anychart.enums.TooltipPositionMode
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_statistics.*


class StatisticsFragment : Fragment(), OnMapReadyCallback {

    private var listener: OnFragmentInteractionListener? = null

    private lateinit var mMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_statistics, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        map_frame.visibility = View.GONE
        location_content.visibility = View.VISIBLE
        progressBar1.progressDrawable = progressBar1.progressDrawable.mutate().apply {
            setColorFilter(ContextCompat.getColor(context!!, R.color.purple), android.graphics.PorterDuff.Mode.SRC_IN)
        }
        swipeRefresh.isEnabled = false

        val cartesian = AnyChart.column()

        val data: List<DataEntry> = mutableListOf(
            ValueDataEntry("Input Instant Sales", 28),
            ValueDataEntry("Input Sales on Credit", 44),
            ValueDataEntry("Input Sales of Saving", 13)
        )

        val column = cartesian.column(data)

        column.labels(true)
        column.labels().format("Ghs {%Value}{groupsSeparator: }000")

        column.tooltip()
            .titleFormat("{%X}")
            .position(Position.CENTER_BOTTOM)
            .anchor(Anchor.CENTER_BOTTOM)
            .offsetX(0.0)
            .offsetY(5.0)
            .format("Ghs {%Value}{groupsSeparator: }000")

        cartesian.animation(true)
//        cartesian.title("Top 10 Cosmetic Products by Revenue")

        cartesian.yScale().minimum(0.0)

        cartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }K")

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT)
        cartesian.interactivity().hoverMode(HoverMode.BY_X)

//        cartesian.xAxis(0).title("Product")
        cartesian.yAxis(0).title("Sales")

        chart.setChart(cartesian)

        location_content.setOnClickListener {
            listener?.showAddLocationView()
        }
    }
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun showLocationOnMap(address: Address) {

        map_frame.visibility = View.VISIBLE
        location_content.visibility = View.GONE

        val where = LatLng(address.latitude, address.longitude)
        mMap.addMarker(MarkerOptions().position(where))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(where, 18f), 200, null)
    }

    interface OnFragmentInteractionListener {
        fun showAddLocationView()
    }

    companion object {
        @JvmStatic
        fun newInstance() = StatisticsFragment()
    }
}
