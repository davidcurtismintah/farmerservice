package com.allow.farmerserviceapp.home.location

import android.content.Context
import android.graphics.Color
import android.location.Address
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.allow.farmerserviceapp.R
import com.allow.farmerserviceapp.home.location.data.ColorSuggestion
import com.allow.farmerserviceapp.home.location.data.DataHelper
import com.arlib.floatingsearchview.FloatingSearchView
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import com.arlib.floatingsearchview.util.Util
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.fragment_location.*


class LocationFragment : Fragment(), AppBarLayout.OnOffsetChangedListener, OnMapReadyCallback {

    interface OnFragmentInteractionListener {
        fun showStatisticsView(address: Address)
    }

    companion object {
        const val TAG = "LocationFragment"
        const val FIND_SUGGESTION_SIMULATED_DELAY: Long = 250
        @JvmStatic
        fun newInstance() = LocationFragment()
    }

    private var listener: OnFragmentInteractionListener? = null

    private lateinit var mSearchView: FloatingSearchView
    private lateinit var mAppBar: AppBarLayout

    private var mIsDarkSearchTheme = false

    private var mLastQuery = ""

    private lateinit var mMap: GoogleMap

    private var mCurrentAddress: Address? = null

    private val mSaveLocationHandler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        mSearchView = view.findViewById(R.id.floating_search_view) as FloatingSearchView
        mAppBar = view.findViewById(R.id.appbar) as AppBarLayout

        mAppBar.addOnOffsetChangedListener(this)

        setupSearchBar()

        save_location.setOnClickListener {
            showSaveLocationLabel()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun setupSearchBar() {
        mSearchView.setOnQueryChangeListener { oldQuery, newQuery ->
            if (oldQuery != "" && newQuery == "") {
                mSearchView.clearSuggestions()
            } else {

                //this shows the top left circular progress
                //you can call it where ever you want, but
                //it makes sense to do it when loading something in
                //the background.
                mSearchView.showProgress()

                //simulates a query call to a data source
                //with a new query.
                DataHelper.findSuggestions(
                    activity, newQuery, 5,
                    FIND_SUGGESTION_SIMULATED_DELAY
                ) { results ->
                    //this will swap the data and
                    //render the collapse/expand animations as necessary
                    mSearchView.swapSuggestions(results)

                    //let the users know that the background
                    //process has completed
                    mSearchView.hideProgress()
                }
            }

            Log.d(TAG, "onSearchTextChanged()")
        }

        mSearchView.setOnSearchListener(object : FloatingSearchView.OnSearchListener {
            override fun onSuggestionClicked(searchSuggestion: SearchSuggestion) {

                val colorSuggestion = searchSuggestion as ColorSuggestion
                DataHelper.findColors(
                    activity, colorSuggestion.body
                ) {
                    //show search results
                    pointToLocation(it)
                }
                Log.d(TAG, "onSuggestionClicked()")

                mLastQuery = searchSuggestion.body
                mSearchView.setSearchFocused(false)

            }

            override fun onSearchAction(query: String) {
                mLastQuery = query

                DataHelper.findColors(
                    activity, query
                ) {
                    //show search results
                    pointToLocation(it)
                }
                mSearchView.setSearchFocused(false)



                Log.d(TAG, "onSearchAction()")
            }
        })

        mSearchView.setOnFocusChangeListener(object : FloatingSearchView.OnFocusChangeListener {
            override fun onFocus() {

                //show suggestions when search bar gains focus (typically history suggestions)
                mSearchView.swapSuggestions(DataHelper.getHistory(activity, 3))

                Log.d(TAG, "onFocus()")
            }

            override fun onFocusCleared() {

                //set the title of the bar so that when focus is returned a new query begins
                mSearchView.setSearchBarTitle(mLastQuery)

                //you can also set setSearchText(...) to make keep the query there when not focused and when focus returns
                //mSearchView.setSearchText(searchSuggestion.getBody());

                Log.d(TAG, "onFocusCleared()")
            }
        })


        //handle menu clicks the same way as you would
        //in a regular activity
        /*mSearchView.setOnMenuItemClickListener { item ->
            if (item.itemId === R.id.action_change_colors) {

                mIsDarkSearchTheme = true

                //demonstrate setting colors for items
                mSearchView.setBackgroundColor(Color.parseColor("#787878"))
                mSearchView.setViewTextColor(Color.parseColor("#e9e9e9"))
                mSearchView.setHintTextColor(Color.parseColor("#e9e9e9"))
                mSearchView.setActionMenuOverflowColor(Color.parseColor("#e9e9e9"))
                mSearchView.setMenuItemIconColor(Color.parseColor("#e9e9e9"))
                mSearchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"))
                mSearchView.setClearBtnColor(Color.parseColor("#e9e9e9"))
                mSearchView.setDividerColor(Color.parseColor("#BEBEBE"))
                mSearchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"))
            } else {

                //just print action
                Toast.makeText(
                    activity!!.applicationContext, item.title,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }*/

        //use this listener to listen to menu clicks when app:floatingSearch_leftAction="showHome"
        mSearchView.setOnHomeActionClickListener { Log.d(TAG, "onHomeClicked()") }

        /*
                * Here you have access to the left icon and the text of a given suggestion
                * item after as it is bound to the suggestion list. You can utilize this
                * callback to change some properties of the left icon and the text. For example, you
                * can load the left icon images using your favorite image loading library, or change text color.
                *
                *
                * Important:
                * Keep in mind that the suggestion list is a RecyclerView, so views are reused for different
                * items in the list.
                */
        mSearchView.setOnBindSuggestionCallback { suggestionView, leftIcon, textView, item, itemPosition ->
            val colorSuggestion = item as ColorSuggestion

            val textColor = if (mIsDarkSearchTheme) "#ffffff" else "#000000"
            val textLight = if (mIsDarkSearchTheme) "#bfbfbf" else "#787878"

            if (colorSuggestion.isHistory) {
                leftIcon.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_history_black_24dp, null
                    )
                )

                Util.setIconColor(leftIcon, Color.parseColor(textColor))
                leftIcon.alpha = .36f
            } else {
                leftIcon.alpha = 0.0f
                leftIcon.setImageDrawable(null)
            }

            textView.setTextColor(Color.parseColor(textColor))
            val text = colorSuggestion.body
                .replaceFirst(
                    mSearchView.query,
                    "<font color=\"" + textLight + "\">" + mSearchView.query + "</font>"
                )
            textView.text = Html.fromHtml(text)
        }
    }


    private fun pointToLocation(it: MutableList<Address>) {
        if (it.size > 0) {
            val address = it[0]
            val where = LatLng(address.latitude, address.longitude)
            mMap.addMarker(MarkerOptions().position(where))
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(where, 18f), 200, null)

            showSaveLocationButton()

            mCurrentAddress = address
        }
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
        mSearchView.translationY = verticalOffset.toFloat()
    }

    fun onActivityBackPress(): Boolean {
        //if mSearchView.setSearchFocused(false) causes the focused search
        //to close, then we don't want to close the activity. if mSearchView.setSearchFocused(false)
        //returns false, we know that the search was already closed so the call didn't change the focus
        //state and it makes sense to call supper onBackPressed() and close the activity
        return mSearchView.setSearchFocused(false)
    }

    fun showSaveLocationButton(){
        save_location.visibility = View.VISIBLE
//        save_location_content.visibility = View.VISIBLE
//        floating_search_view.visibility = View.GONE
    }

    fun showSaveLocationLabel(){
        save_location.visibility = View.VISIBLE
        shadow.visibility = View.VISIBLE
        save_location_content.visibility = View.VISIBLE
        floating_search_view.visibility = View.GONE

        mSaveLocationHandler.postDelayed({
            save_location_label.text = getString(R.string.save_location_done_text)

            mSaveLocationHandler.postDelayed({
                mCurrentAddress?.let { listener?.showStatisticsView(it) }

            }, 2000)

        }, 2000)
    }

}
