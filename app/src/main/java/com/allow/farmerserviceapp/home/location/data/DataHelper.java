package com.allow.farmerserviceapp.home.location.data;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.widget.Filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DataHelper {

    private static List<ColorSuggestion> sColorSuggestions = new ArrayList<>();

    public interface OnFindColorsListener {
        void onResults(List<Address> results);
    }

    public interface OnFindSuggestionsListener {
        void onResults(List<ColorSuggestion> results);
    }

    public static List<ColorSuggestion> getHistory(Context context, int count) {

        List<ColorSuggestion> suggestionList = new ArrayList<>();
        ColorSuggestion colorSuggestion;
        for (int i = 0; i < sColorSuggestions.size(); i++) {
            colorSuggestion = sColorSuggestions.get(i);
            colorSuggestion.setIsHistory(true);
            suggestionList.add(colorSuggestion);
            if (suggestionList.size() == count) {
                break;
            }
        }
        return suggestionList;
    }

    public static void resetSuggestionsHistory() {
        for (ColorSuggestion colorSuggestion : sColorSuggestions) {
            colorSuggestion.setIsHistory(false);
        }
    }

    public static void findSuggestions(final Context context, String query, final int limit, final long simulatedDelay,
                                       final OnFindSuggestionsListener listener) {
        new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                Geocoder geocoder = new Geocoder(context);
                try {
                    if (!(constraint == null)){
                        List<Address> addressList = geocoder.getFromLocationName(constraint.toString(), limit);
                        for (Address address: addressList) {
                            ColorSuggestion colorSuggestion = new ColorSuggestion(address.getAddressLine(0));
                            if (!sColorSuggestions.contains(colorSuggestion)) {
                                sColorSuggestions.add(colorSuggestion);
                            }
                        }
                    }
                } catch (IOException e){
                    e.printStackTrace();
                }

                FilterResults results = new FilterResults();
                Collections.sort(sColorSuggestions, new Comparator<ColorSuggestion>() {
                    @Override
                    public int compare(ColorSuggestion lhs, ColorSuggestion rhs) {
                        return lhs.getIsHistory() ? -1 : 0;
                    }
                });
                results.values = sColorSuggestions;
                results.count = sColorSuggestions.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (listener != null) {
                    listener.onResults((List<ColorSuggestion>) results.values);
                }
            }
        }.filter(query);

    }


    public static void findColors(final Context context, String query, final OnFindColorsListener listener) {
//        initColorWrapperList(context);

        new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {


                Geocoder geocoder = new Geocoder(context);
                List<Address> suggestionList = new ArrayList<>();
                try {
                    if (!(constraint == null)){
                        suggestionList = geocoder.getFromLocationName(constraint.toString(), 1);
                    }
                } catch (IOException e){
                    e.printStackTrace();
                }

                FilterResults results = new FilterResults();
                results.values = suggestionList;
                results.count = suggestionList.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (listener != null) {
                    listener.onResults((List<Address>) results.values);
                }
            }
        }.filter(query);

    }
}