package com.allow.farmerserviceapp.authenticate

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.allow.farmerserviceapp.R
import com.allow.farmerserviceapp.authenticate.facebookSignIn.FacebookHelper
import com.allow.farmerserviceapp.authenticate.facebookSignIn.FacebookResponse
import com.allow.farmerserviceapp.authenticate.facebookSignIn.FacebookUser
import com.allow.farmerserviceapp.authenticate.googleAuthSignin.GoogleAuthResponse
import com.allow.farmerserviceapp.authenticate.googleAuthSignin.GoogleAuthUser
import com.allow.farmerserviceapp.authenticate.googleAuthSignin.GoogleSignInHelper
import com.allow.farmerserviceapp.authenticate.linkedInSiginIn.LinkedInHelper
import com.allow.farmerserviceapp.authenticate.linkedInSiginIn.LinkedInResponse
import com.allow.farmerserviceapp.authenticate.linkedInSiginIn.LinkedInUser
import com.allow.farmerserviceapp.home.HomeActivity
import kotlinx.android.synthetic.main.activity_authenticate.*


class AuthActivity : AppCompatActivity(), View.OnClickListener,
    FacebookResponse,
    LinkedInResponse,
    GoogleAuthResponse {

    private lateinit var mFbHelper: FacebookHelper
    private lateinit var mGAuthHelper: GoogleSignInHelper
    private lateinit var mLinkedInHelper: LinkedInHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_authenticate)

        //google auth initialization
        mGAuthHelper = GoogleSignInHelper(this, null, this)

        //fb api initialization
        mFbHelper = FacebookHelper(
            this,
            "id,name,email,gender,birthday,picture,cover",
            this
        )

        //linkedIn initializer
        mLinkedInHelper = LinkedInHelper(this, this)

        //set sign in button
        g_login_btn.setOnClickListener(this)
        bt_act_login_fb.setOnClickListener(this)
        linkedin_login_button.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.g_login_btn -> mGAuthHelper.performSignIn(this)
            R.id.bt_act_login_fb -> mFbHelper.performSignIn(this)
            R.id.linkedin_login_button -> mLinkedInHelper.performSignIn()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //handle results
        mFbHelper.onActivityResult(requestCode, resultCode, data)
        mGAuthHelper.onActivityResult(requestCode, resultCode, data)
        mLinkedInHelper.onActivityResult(requestCode, resultCode, data)
    }

    override fun onFbSignInFail() {
        Toast.makeText(this, "Facebook sign in failed.", Toast.LENGTH_SHORT).show()
    }

    override fun onFbSignInSuccess() {
        Toast.makeText(this, "Facebook sign in success", Toast.LENGTH_SHORT).show()
        startHomeActivity()
    }

    override fun onFbProfileReceived(facebookUser: FacebookUser) {
        Toast.makeText(
            this,
            "Facebook user data: name= " + facebookUser.name + " email= " + facebookUser.email,
            Toast.LENGTH_SHORT
        ).show()

        Log.d("Person name: ", facebookUser.name + "")
        Log.d("Person gender: ", facebookUser.gender + "")
        Log.d("Person email: ", facebookUser.email + "")
        Log.d("Person image: ", facebookUser.facebookID + "")
    }

    override fun onFBSignOut() {
        Toast.makeText(this, "Facebook sign out success", Toast.LENGTH_SHORT).show()
    }

    override fun onLinkedInSignInFail() {
        Toast.makeText(this, "LinkedIn sign in failed.", Toast.LENGTH_SHORT).show()
    }

    override fun onLinkedInSignInSuccess(accessToken: String) {
        Toast.makeText(this, "Linked in signin successful.\n Getting user profile...", Toast.LENGTH_SHORT).show()
        startHomeActivity()
    }

    override fun onLinkedInProfileReceived(user: LinkedInUser) {
        Toast.makeText(this, "LinkedIn user data: name= " + user.name + " email= " + user.email, Toast.LENGTH_SHORT)
            .show()
    }

    override fun onGoogleAuthSignIn(user: GoogleAuthUser) {
        Toast.makeText(this, "Google user data: name= " + user.name + " email= " + user.email, Toast.LENGTH_SHORT)
            .show()
        startHomeActivity()
    }

    override fun onGoogleAuthSignInFailed() {
        Toast.makeText(this, "Google sign in failed.", Toast.LENGTH_SHORT).show()
    }

    override fun onGoogleAuthSignOut(isSuccess: Boolean) {
        Toast.makeText(this, if (isSuccess) "Sign out success" else "Sign out failed", Toast.LENGTH_SHORT).show()
    }

    private fun startHomeActivity() {
        startActivity(HomeActivity.startIntent(this))
        finish()
    }

}
